 package ir.didehvar.placement.ui.list

import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.IBinder
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import ir.didehvar.placement.utils.logT
import java.lang.Exception

class LocationService : BroadcastReceiver() {

    companion object{
        val ACTION_UPDATE = "location_update"

    }

    override fun onReceive(context: Context?, intent: Intent?) {
        if(intent != null) {
            when(intent.action){
                ACTION_UPDATE -> {
                    getLocationResult(intent)
                }
            }
        }
    }

    private fun getLocationResult(intent: Intent?) {
        val result = LocationResult.extractResult(intent)
        if(result != null){
            val res = result.lastLocation
            try {
                val cur = ListFragment.instance?.location?.value
//                if(cur != null && res.distanceTo(cur) > 100)
                    ListFragment.instance?.location?.postValue(res)
                    logT(res.latitude.toString() + ", " + res.longitude.toString())
            }
            catch (e:Exception){
            }
        }
    }
}