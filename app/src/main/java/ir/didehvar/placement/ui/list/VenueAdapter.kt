package ir.didehvar.placement.ui.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import ir.didehvar.placement.data.model.Explore
import ir.didehvar.placement.databinding.LayoutVenueCardBinding
import ir.didehvar.placement.utils.logT

class VenueAdapter(private val onClickListener: (Explore.Response.Group.Item.Venue) -> Unit) :
    RecyclerView.Adapter<VenueAdapter.Holder>() {

    private var items: ArrayList<Explore.Response.Group.Item.Venue> = ArrayList()

    class Holder(val binding: LayoutVenueCardBinding) : RecyclerView.ViewHolder(binding.root)
    {
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val binding = LayoutVenueCardBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return Holder(binding)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.binding.root.setOnClickListener { onClickListener(items[position]) }
        holder.binding.txtName.text = items[position].name
    }

    override fun getItemCount() = items.size

    fun addItems(venus: List<Explore.Response.Group.Item.Venue>){
        var initSize = items.size
        items.addAll(venus)
        notifyItemRangeInserted(initSize, venus.size)
    }
}