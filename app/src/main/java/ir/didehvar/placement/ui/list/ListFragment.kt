package ir.didehvar.placement.ui.list


import android.Manifest
import android.app.PendingIntent
import android.app.PendingIntent.FLAG_UPDATE_CURRENT
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.os.bundleOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import dagger.hilt.android.AndroidEntryPoint
import ir.didehvar.placement.R
import ir.didehvar.placement.core.BaseFragment
import ir.didehvar.placement.data.model.Explore
import ir.didehvar.placement.databinding.FragmentListBinding
import ir.didehvar.placement.ui.list.LocationService.Companion.ACTION_UPDATE
import ir.didehvar.placement.utils.*
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions
import java.net.CacheRequest

@AndroidEntryPoint
class ListFragment :
    BaseFragment<ListFragmentViewModel, FragmentListBinding>(ListFragmentViewModel::class.java),
    EasyPermissions.PermissionCallbacks {

    lateinit var locationRequest: LocationRequest
    lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    lateinit var location: MutableLiveData<Location>
    private var current_page = 0

    private lateinit var ll: String

    companion object {
        var instance: ListFragment? = null

        fun getMainInstance(): ListFragment {
            return instance!!
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        instance = this
        location = MutableLiveData()
    }

    override fun getLayoutRes() = R.layout.fragment_list

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requestPermissions()

        updateLocation()

        mBinding.rcvVenues.apply {
            val laymgr = LinearLayoutManager(this.context)
            this.layoutManager = laymgr
            setHasFixedSize(true)
            addOnScrollListener(InfinityScrolling({
                current_page++
                requestData()
            }, laymgr))
            this.adapter = VenueAdapter{
                val bundle = bundleOf(DETAIL_FRAGMENT_ARGUMENT to it.id)
                findNavController().navigate(R.id.detailFragment, bundle)
            }
            // This a simple divider between each list item in the RecyclerView
            this.addItemDecoration(
                DividerItemDecoration(
                    this.context,
                    (this.layoutManager as LinearLayoutManager).orientation
                )
            )
        }

        location.observe(viewLifecycleOwner) { it ->
            ll = it.latitude.toString() + "," + it.longitude.toString()
            requestData()
        }

    }

    private fun requestData() {
        viewModel.venueListLiveData(ll, current_page * OFFSET)?.observe(viewLifecycleOwner) {
            val position = mBinding.rcvVenues.childCount
            (mBinding.rcvVenues.adapter as VenueAdapter).addItems(it)
            (mBinding.rcvVenues.adapter as VenueAdapter).notifyDataSetChanged()
//            mBinding.rcvVenues.scrollToPosition(position - OFFSET)
        }
    }

    private fun requestPermissions() {
        if (hasLocationPermission(requireContext())) {
            return
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
            EasyPermissions.requestPermissions(
                this,
                "Please Accept Location Permission...",
                REQUEST_LOCATION_PERMISSION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_BACKGROUND_LOCATION
            )
        }
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            AppSettingsDialog.Builder(this).build().show()
        } else {
            requestPermissions()
        }
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {}

    private fun updateLocation() {
        buildLocationRequest()
        fusedLocationProviderClient =
            LocationServices.getFusedLocationProviderClient(requireContext())
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, getPendingIntent())
    }

    private fun getPendingIntent(): PendingIntent? {
        val intent = Intent(requireContext(), LocationService::class.java)
        intent.action = ACTION_UPDATE
        return PendingIntent.getBroadcast(requireContext(), 0, intent, FLAG_UPDATE_CURRENT)
    }

    private fun buildLocationRequest() {
        locationRequest = LocationRequest()
        locationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        locationRequest.interval = 5000
        locationRequest.fastestInterval = 3000
        locationRequest.smallestDisplacement = 10f
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

}