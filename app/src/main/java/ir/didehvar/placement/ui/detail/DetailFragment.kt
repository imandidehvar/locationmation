package ir.didehvar.placement.ui.detail

import android.os.Bundle
import android.view.View
import androidx.lifecycle.observe
import com.bumptech.glide.RequestManager
import dagger.hilt.android.AndroidEntryPoint
import ir.didehvar.placement.R
import ir.didehvar.placement.core.BaseFragment
import ir.didehvar.placement.databinding.FragmentDetailBinding
import saman.zamani.persiandate.PersianDate
import saman.zamani.persiandate.PersianDateFormat
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class DetailFragment: BaseFragment<DetailFragmentViewModel, FragmentDetailBinding>(DetailFragmentViewModel::class.java) {

    @Inject
    lateinit var glide: RequestManager

    override fun getLayoutRes() = R.layout.fragment_detail

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var id = arguments?.getString("venue_id").toString()

        viewModel.detailsLiveData(id).observe(viewLifecycleOwner){
            if(it != null){
                mBinding.txtName.text = it.name
                mBinding.txtRating.text = it.rating.toString()
//                glide.load()

                val date = Date(it.createdAt.toLong())
                val formater = SimpleDateFormat("dd/MM/yyyy")
                mBinding.txtCreatedAt.text = formater.format(date)

            }
        }
    }
}