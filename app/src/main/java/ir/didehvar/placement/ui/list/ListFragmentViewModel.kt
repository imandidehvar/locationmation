package ir.didehvar.placement.ui.list

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ir.didehvar.placement.core.BaseViewModel
import ir.didehvar.placement.data.VenueRepository
import ir.didehvar.placement.data.local.Database
import ir.didehvar.placement.data.model.Explore
import ir.didehvar.placement.utils.logT
import kotlinx.coroutines.Dispatchers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

class ListFragmentViewModel @ViewModelInject constructor(private val venueRepository: VenueRepository) : BaseViewModel() {

    fun venueListLiveData(ll: String, offset: Int): LiveData<List<Explore.Response.Group.Item.Venue>>? {
        return venueRepository.getAllVenueLiveData(ll,offset)
    }

}