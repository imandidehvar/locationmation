package ir.didehvar.placement.ui.detail

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import ir.didehvar.placement.core.BaseViewModel
import ir.didehvar.placement.data.VenueRepository
import ir.didehvar.placement.data.model.Details

class DetailFragmentViewModel @ViewModelInject constructor(private val venueRepository: VenueRepository) : BaseViewModel() {

    fun detailsLiveData(venue_id: String): LiveData<Details.Response.Detail> {
        return venueRepository.getVenueDetailLiveData(venue_id)
    }
}