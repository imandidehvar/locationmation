package ir.didehvar.placement.ui.main

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import com.google.android.gms.location.LocationServices
import dagger.hilt.android.AndroidEntryPoint
import ir.didehvar.placement.R
import ir.didehvar.placement.core.BaseActivity
import ir.didehvar.placement.data.VenueRepository
import ir.didehvar.placement.data.model.Explore
import ir.didehvar.placement.databinding.ActivityMainBinding
import ir.didehvar.placement.utils.SHOW_LIST_FRAGMENT
import javax.inject.Inject


@AndroidEntryPoint
class MainAcitivity: BaseActivity<MainActivityViewModel, ActivityMainBinding>(MainActivityViewModel::class.java) {

    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setDisplayShowCustomEnabled(true);
        setSupportActionBar(binding.toolbar)

    }

    override fun getLayoutRes() = R.layout.activity_main

    override fun initViewModel(viewModel: MainActivityViewModel) {
    }
}