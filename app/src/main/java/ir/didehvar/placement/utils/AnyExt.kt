package ir.didehvar.placement.utils

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Build
import android.util.Log
import pub.devrel.easypermissions.EasyPermissions
import java.util.jar.Manifest

fun Any.logE(message: String) = Log.e(this::class.java.simpleName, message)

fun Any.logD(message: String) = Log.d(this::class.java.simpleName, message)

fun Any.logV(message: String) = Log.v(this::class.java.simpleName, message)

fun Any.logW(message: String) = Log.w(this::class.java.simpleName, message)

fun Any.logI(message: String) = Log.i(this::class.java.simpleName, message)

fun Any.logT(message: String) = Log.i(this::class.java.simpleName + " Test=>", message)

fun Any.emptyString() = ""

fun hasLocationPermission(context: Context) = if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
    EasyPermissions.hasPermissions(
        context,
        android.Manifest.permission.ACCESS_COARSE_LOCATION,
        android.Manifest.permission.ACCESS_COARSE_LOCATION
    )
} else {
    EasyPermissions.hasPermissions(
        context,
        android.Manifest.permission.ACCESS_COARSE_LOCATION,
        android.Manifest.permission.ACCESS_COARSE_LOCATION,
        android.Manifest.permission.ACCESS_BACKGROUND_LOCATION
    )
}

@SuppressLint("ServiceCast")
fun isConnectedToInternet(context: Context): Boolean {
    val connectivity = context.getSystemService(
        Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    if (connectivity != null) {
        val info = connectivity.allNetworkInfo
        if (info != null)
            for (i in info.indices)
                if (info[i].state == NetworkInfo.State.CONNECTED) {
                    return true
                }
    }
    return false
}