package ir.didehvar.placement.data


import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import ir.didehvar.placement.data.local.Database
import ir.didehvar.placement.data.model.Details
import ir.didehvar.placement.data.model.Explore
import ir.didehvar.placement.data.remote.Api
import ir.didehvar.placement.utils.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

class VenueRepository @Inject constructor(private val database: Database, private val api: Api) {

    fun getAllVenueLiveData(ll: String, offset: Int): LiveData<List<Explore.Response.Group.Item.Venue>> {
        getAllVenuesFromRemote(ll)

        return database.getVenueDao().getVenues(LIMIT, offset)
    }

    fun getVenueDetailLiveData(venue_id: String): LiveData<Details.Response.Detail> {
        val t = getVenueDetailsFromRemote(venue_id)
        return database.getDetailDao().getDetail(venue_id)

    }

    private fun getAllVenuesFromRemote(ll: String): LiveData<List<Explore.Response.Group.Item>> {
        var list: MutableLiveData<List<Explore.Response.Group.Item>> = MutableLiveData()

        api.getlist(CLIENT_ID, CLIENT_SECRET, ll).enqueue(object :
            Callback<Explore> {
            override fun onFailure(call: Call<Explore>, t: Throwable) {
                logT(t.message.toString())
            }

            override fun onResponse(call: Call<Explore>, response: Response<Explore>) {
                Observable.just(database)
                    .subscribeOn(Schedulers.io())
                    .subscribe { db ->
                        database.getVenueDao().deleteAll()
                        response.body()?.response!!.groups[0].items.forEach {
                            db.getVenueDao().insert(it.venue)
                        }
                    }
            }

        })

        return list
    }

    private fun getVenueDetailsFromRemote(venue_id: String): LiveData<Details.Response.Detail> {
        var list: MutableLiveData<Details.Response.Detail> = MutableLiveData()
        api.getDetails(venue_id, CLIENT_ID, CLIENT_SECRET).enqueue(object : Callback<Details> {
            override fun onFailure(call: Call<Details>, t: Throwable) {
                logT(t.message.toString())
            }

            override fun onResponse(call: Call<Details>, response: Response<Details>) {
                logT(call.request().url().toString())
                Observable.just(database)
                    .subscribeOn(Schedulers.io())
                    .subscribe { db ->
                        db.getDetailDao().deleteDetail(response.body()?.response!!.venue)
                        db.getDetailDao().insertDetail(response.body()?.response!!.venue)
                    }
            }

        })

        return list
    }

}