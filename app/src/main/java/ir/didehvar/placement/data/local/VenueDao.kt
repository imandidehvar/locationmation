package ir.didehvar.placement.data.local


import androidx.lifecycle.LiveData
import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Observable
import ir.didehvar.placement.data.model.Explore

@Dao
interface VenueDao {

    @Query("SELECT * FROM Venue ORDER BY id DESC  limit :limit offset :offset")
    fun getVenues(limit: Int, offset: Int): LiveData<List<Explore.Response.Group.Item.Venue>>

    @Query("SELECT * FROM Venue WHERE id = :venue ORDER BY id DESC")
    fun getVenue(venue: String): LiveData<List<Explore.Response.Group.Item.Venue>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert( venues: Explore.Response.Group.Item.Venue)

    @Delete
    fun deleteVenue(venue: Explore.Response.Group.Item.Venue)

    @Query("DELETE FROM venue")
    fun deleteAll()

}