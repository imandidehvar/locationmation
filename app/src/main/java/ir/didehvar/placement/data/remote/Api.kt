package ir.didehvar.placement.data.remote

import io.reactivex.Observable
import ir.didehvar.placement.data.model.Details
import ir.didehvar.placement.data.model.Explore
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface Api {

    @GET("venues/explore?v=20201101")
    fun getlist(@Query("client_id") client_id: String, @Query("client_secret") client_secret: String, @Query("ll") ll: String): Call<Explore>

    @GET("venues/{id}?v=20201101")
    fun getDetails(@Path("id") venue_id: String, @Query("client_id") client_id: String, @Query("client_secret") client_secret: String): Call<Details>
}