package ir.didehvar.placement.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import ir.didehvar.placement.data.model.Details
import ir.didehvar.placement.data.model.Explore

@Database(entities = [Explore.Response.Group.Item.Venue::class, Details.Response.Detail::class], version = 1, exportSchema = false)
abstract class Database: RoomDatabase() {
    abstract fun getVenueDao(): VenueDao
    abstract fun getDetailDao(): DetailDao
}