package ir.didehvar.placement.data.local

import androidx.lifecycle.LiveData
import androidx.room.*
import ir.didehvar.placement.data.model.Details

@Dao
interface DetailDao {

    @Query("SELECT * FROM detail")
    fun getAllRecords(): List<Details.Response.Detail>

    @Query("SELECT * FROM Detail WHERE id = :venue_id")
    fun getDetail(venue_id: String): LiveData<Details.Response.Detail>

    @Delete
    fun deleteDetail(detail: Details.Response.Detail)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDetail(detail: Details.Response.Detail)
}