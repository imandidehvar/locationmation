package ir.didehvar.placement.data.model

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import javax.inject.Inject

data class Meta(
    val code: Int,
    val requestId: String
)

data class Explore(
    @SerializedName("meta")
    val meta: Meta,
    @SerializedName("response")
    val response: Response

) {
    data class Response(
        @SerializedName("groups")
        val groups: List<Group>
    ) {
        data class Group(
            @SerializedName("type") val type : String,
            @SerializedName("name") val name : String,
            @SerializedName("items") val items: List<Item>
        ) {
            data class Item(
                @SerializedName("venue") val venue : Venue
            ) {
                @Entity(tableName = "venue")
                data class Venue(
                    @PrimaryKey
                    val id: String,
                    val name: String
//                    @Ignore
//                    val categories: List<Category>
                ) {
                    data class Category(
                        val id: String,
                        val name: String,
                        val primary: Boolean
                    )
                }
            }
        }
    }
}

data class Details(
    @SerializedName("meta")
    val meta: Meta,
    @SerializedName("response")
    val response: Response
){
    data class Response(
        @SerializedName("venue")
        val venue: Detail
    ){
        @Entity(tableName = "detail")
        data class Detail(
            @PrimaryKey
            val id: String,
            val name: String,
            val rating: Float,
            val createdAt: Int
        )
    }
}