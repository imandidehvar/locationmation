package ir.didehvar.placement.di

import android.content.Context
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import ir.didehvar.placement.R
import ir.didehvar.placement.data.VenueRepository
import ir.didehvar.placement.data.local.Database
import ir.didehvar.placement.data.remote.Api
import retrofit2.Retrofit
import javax.inject.Singleton

@InstallIn(ApplicationComponent::class)
@Module
object ApplicationModule {

    @Singleton
    @Provides
    fun provideVenueRepository(database: Database, api: Api) : VenueRepository = VenueRepository(database, api)

    @Singleton
    @Provides
    fun provideGlide(@ApplicationContext context: Context) = Glide.with(context).setDefaultRequestOptions(
        RequestOptions()
            .diskCacheStrategy(DiskCacheStrategy.DATA)
            .placeholder(R.mipmap.notfound)
            .error(R.mipmap.notfound)
    )

}